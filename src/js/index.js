let conatiner = require('./services/cotainer')

let workerPool = conatiner.workerPool

console.log(workerPool)

window.requestAnimFrame = (function(){
    return function( callback ){
        let ratio = Math.sin(conatiner.tick().timeElapsedSec / 5)
        // 24.5 fps like n the movies
        // let waitBase = 150
        // let wait = waitBase - Math.abs(((waitBase - 10) * ratio))
        // console.log(wait)
        let wait = 41
        window.setTimeout(callback, wait);
    };
})();

module.exports = exports = function (w = 256, h = 144, patriclesNum = 60) {

    let Bottle = require('bottlejs')


    let canvasBuffer = conatiner.canvas
    // let canvasBuffer002 = document.querySelector('#canvas-buffer-002')
    let canvas = document.querySelector('#canvas')

    let ctx = canvasBuffer.getContext('2d')
    let ctxMain = canvas.getContext('2d')

    let particles = []
    let colors = conatiner.getBasicColors

    // canvas.width = window.innerWidth
    // canvas.height = window.innerHeight
    canvas.width = w
    canvas.height = h

    // canvas.style.left = 0 + 'px';

    let setCanvas = (buffer) =>{
        buffer.width = w
        buffer.height = h
        // buffer.style.left = canvas.style.left;
    }

    setCanvas(canvasBuffer)
    canvasBuffer.width = canvas.width
    canvasBuffer.height = canvas.height
    canvasBuffer.style.left =canvas.style.left;

    let logger2 = conatiner.throttledLoggerFactory(1000)

    function Factory(){
        let bottle = new Bottle()
        bottle.service('constants', function () {
            let xStart = Math.round( Math.random() * w)
            let yStart = Math.round( Math.random() * h)
            let xStartP = Math.random()
            let yStartP = Math.random()
            let interpolationCanvas = function (tick = null, xStartP, vps, pixels, ratio =1) {
                tick = tick || conatiner.tick()
                let position = Math.round(xStartP * pixels + vps * tick.timeElapsedSec * pixels * (ratio))
                let mod = position%pixels
                let floor = Math.floor(position/pixels)
                let position2 = position - (floor * pixels)

                return position2
            }

            let x = function (tick = null) {
                return interpolationCanvas(tick, this.xStartP, this.vxps, w, h/w *1.5)
            }
            let y = function (tick = null) {
                return interpolationCanvas(tick, this.yStartP, this.vyps, h, 1.5)
            }
            let rad = Math.round( Math.random() * 7);
            let rgba = colors[ Math.round( Math.random() * colors.length-1) ];
            let vx = Math.round( Math.random() * 1) - 1;
            let vy = Math.round( Math.random() * 1) - 1;
            let maxSpeed = 0.4
            let speedInit = (speed) => {
                let ret = (Math.random() * speed) - speed/2;
                return ret
            }
            let vxps = speedInit(maxSpeed);
            let vyps = speedInit(maxSpeed);

            return {
                x,y,
                rad,rgba,
                vx,vy,
                xStart,yStart,
                xStartP,yStartP,
                vxps,vyps
            }
        })

        return bottle.container
    }

    let logger = conatiner.throttledLoggerFactory(1000)

    function draw(tick){
        tick = tick || conatiner.tick()
        // if (!Math.floor((Math.random() * 1000)))console.log(tick)
        let timeElapsed = tick.timeElapsedSec
        logger(tick)
        ctx.clearRect(0, 0, w, h);
        ctx.globalCompositeOperation = 'lighter';
        for(let i = 0;i < patriclesNum; i++){
            let temp = particles[i].constants;
            // console.log(temp)
            let factor = 1;

            for(let j = 0; j<patriclesNum; j++){

                let temp2 = particles[j].constants;
                ctx.linewidth = 0.5;

                if(temp.rgba == temp2.rgba && findDistance(temp, temp2, tick)<2000){
                    ctx.strokeStyle = temp.rgba;
                    ctx.beginPath();
                    ctx.moveTo(temp.x(tick), temp.y(tick));
                    ctx.lineTo(temp2.x(tick), temp2.y(tick));
                    // ctx.filter = 'blur(20px)';
                    ctx.moveTo(temp.x(tick)+1, temp.y(tick)+1);
                    ctx.lineTo(temp2.x(tick)+1, temp2.y(tick)+1);
                    // ctx.filter = 'blur(20px)';
                    ctx.moveTo(temp.x(tick)+2, temp.y(tick)+2);
                    ctx.lineTo(temp2.x(tick)+2, temp2.y(tick)+2);
                    // ctx.filter = 'blur(20px)';
                    ctx.stroke();
                    // ctx.closePath();
                    factor++;
                }
            }

            ctx.fillStyle = temp.rgba;
            ctx.strokeStyle = temp.rgba;

            ctx.beginPath();
            ctx.arc(temp.x(tick), temp.y(tick), temp.rad*factor, 0, Math.PI*2, true);
            ctx.fill();
            // ctx.closePath();

            // ctx.beginPath();
            ctx.arc(temp.x(tick), temp.y(tick), (temp.rad+5)*factor, 0, Math.PI*2, true);

            let blur = Math.floor( (Math.sin(tick.timeElapsedSec) / 2) * 30 )

            ctx.filter = `blur(${blur}px)`;
            // ctx.filter = `hue-rotate(2cd so  0)`;

            ctx.stroke();
            ctx.closePath();

            // canvasBuffer002.shadowBlur = 10;
            // canvasBuffer002.drawImage(ctxMain, 0, 0)
            // canvasBuffer002.drawImage(canvasBuffer, 0, 0)

            // logger2(ctxMain.width, ctxMain.height)

            ctxMain.fillRect(0,0,w, h);

            ctxMain.drawImage(canvasBuffer, 0, 0);
            // temp.x += temp.vx;
            // temp.y += temp.vy;

            // temp.x = temp.vx * sin(tick.timeElapsedSec) * tick.timeElapsedSec * 1000;
            // temp.y = temp.vy * tick.timeElapsedSec * 1000;


            // if(temp.x > w)temp.x = 0;
            // if(temp.x < 0)temp.x = w;
            // if(temp.y > h)temp.y = 0;
            // if(temp.y < 0)temp.y = h;
            //
            // if(temp.x > w)temp.vx *= -1;
            // if(temp.x < 0)temp.vx *= -1;
            // if(temp.y > h)temp.vy *= -1;
            // if(temp.y < 0)temp.vy *= -1;
        }
    }

    function findDistance(p1, p2, tick){
        let res = Math.sqrt( Math.pow(p2.x(tick) - p1.x(tick), 2) + Math.pow(p2.y(tick) - p1.y(tick), 2) );
        return res
    }


    (function init(){
        for(let i = 0; i < patriclesNum; i++){
            particles.push(new Factory);
        }
    })();

    (function loop(tick){
        tick = tick || conatiner.tick()
        draw(tick);
        requestAnimFrame(loop);
    })();
}(undefined, undefined, 30)
