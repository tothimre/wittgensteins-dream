let permutations = require('get-permutations')

module.exports = exports = function(){
    // let arr = permutations(['ff', '99', '64'])
    let arr = permutations(['64','ff', '44'])
    arr = arr.map(function (arry) {
        return '#' + arry.join('')
    })
    let c = new Array(...arr)
    return c
}

