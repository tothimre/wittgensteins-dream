let _ = require('lodash')
module.exports = exports = function () {

    return function (millisecs) {
        let th = _.throttle(function(){
            console.log(...arguments)
        }, millisecs)
        return function () {
            th(...arguments)
        }

    }
}