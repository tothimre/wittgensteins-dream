let Bottle = require('bottlejs')
let Timer = require('timer-machine')

module.exports = function () {
  let bottle = new Bottle()
  let container = bottle.container

  bottle.service('timer', function () {
    return new Timer()
  })

  bottle.service('getBasicColors', require('./get-basic-colors'))
  bottle.service('throttledLoggerFactory', require('./throttled-logger-factory'))
  bottle.service('tick', require('./tick'), 'timer')
  bottle.service('interploation', require('./interpolation'))

    container.workerPool = require('workerpool');

    bottle.service('canvas', function () {
        let canvas = document.querySelector('#canvas-buffer')
        // canvas.width = window.innerWidth
        // canvas.height = window.innerHeight

        canvas.width = 640
        canvas.height = 480

        return canvas
    })

    bottle.service('getCanvas2dCtx', function (canvas) {

        return canvas.getContext('2d')
    }, 'canvas')

    container.makeid = function makeid() {
        let text = "";
        let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 5; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }

  bottle.service('get2dContext', function (canvas) {
      return function () {
          // let rand = container.makeid();
          let newCanvasElement = document.createElement("canvas");
          // newCanvas.id = rand;
          // document.appendChild(newCanvasElement);
          newCanvasElement.height = 640
          newCanvasElement.width = 360

          return newCanvasElement.getContext('2d')
      }
      // let canvasDOM = document.querySelector('#canvas')
  }, canvas)

  container.timer.start()

  return container
}()