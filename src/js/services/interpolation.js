/**
Times are seconds
*/

let simple = {
    byValue: function (deltaPerSecond, startTime, currentTime, offsetValue = 0) {
        let timeElapsed = currentTime - startTime
        let currentValue = deltaPerSecond * timeElapsed

        return currentValue + offsetValue
    },

    byTime: function (deltaPerSecond, destinationValue, currentValue, offsetValue = 0) {
        let deltaValue = (offsetValue + currentValue) - destinationValue
        let deltaTime = deltaValue / deltaPerSecond

        return deltaTime
    }
}


module.exports = exports =
{
    simple
}
