module.exports = exports = function (timer) {
    let timeFlow = [0,0]
    return function () {
        let frameTimeDiff = function(){
            timeFlow.push(timer.time())
            let ret = timeFlow[timeFlow.length-1] - timeFlow[timeFlow.length-2]
            timeFlow = timeFlow.slice(1,3)
            return ret
        }()

        let fps = Math.round((1000/frameTimeDiff)*100)/100
        let timeElapsed = timeFlow[timeFlow.length-1]
        let timeElapsedSec =
            // Math.floor
            (timeElapsed / 1000);
        let timeElapsedMin =
            // Math.floor
            (timeElapsedSec / 60);
        let timeElapsedHour =
            // Math.floor
            (timeElapsedMin / 60);
        let timeElapsedDay =
            // Math.floor
            (timeElapsedHour / 24);

        return{
            frameTimeDiff,
            fps,
            timeElapsed,
            timeElapsedSec,
            timeElapsedMin,
            timeElapsedDay,
            // timeElapsedMin,
        }
    }
}